package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void translatorShouldReturnInputPhrase() {
		String inputPhrase = "hello world";
		Translator t = new Translator(inputPhrase);
		
		assertEquals(inputPhrase, t.getPhrase());
	}
	
	@Test
	public void testTranslationForEmptyPhrase() {
		String inputPhrase = "";
		Translator t = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL, t.translate());
	}
	
	@Test
	public void testTranslationForPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("anynay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseStartingWithAEndingWithE() {
		String inputPhrase = "apple";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("appleyay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseStartingWithAEndingWithK() {
		String inputPhrase = "ask";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("askay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ownknay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithSpace() {
		String inputPhrase = "hello world";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithDash() {
		String inputPhrase = "well-being";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithDashAndSpace() {
		String inputPhrase = "happiniess gives us well-being";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("appiniesshay ivesgay usay ellway-eingbay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithExclamationPoint() {
		String inputPhrase = "hello world!";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithPoint() {
		String inputPhrase = "hello world.";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway.", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithComma() {
		String inputPhrase = "hello, world";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay, orldway", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithRoundParenthesis() {
		String inputPhrase = "(hello) world";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("(ellohay) orldway", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithApostrophe() {
		String inputPhrase = "apple isn't pear";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("appleyay isnay'tay earpay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithAnUpperCaseWord() {
		String inputPhrase = "APPLE";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("APPLEYAY", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithATitleCaseWord() {
		String inputPhrase = "Hello";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("Ellohay", t.translate());
	}
	
	@Test
	public void testTranslationForPhraseWithANonUpperOrTitleCaseWord() {
		String inputPhrase = "biRd";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("iRdbay", t.translate());
	}
}
