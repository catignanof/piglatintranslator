package piglatintranslator;

public class Translator {
	
	public static final String NIL = "nil";
	
	private static final String SPACE = " ";
	private static final String DASH = "-";
	
	private String inputPhrase;
	
	
	public Translator(String inputPhrase) {
		this.inputPhrase = inputPhrase;
	}
	

	public String getPhrase() {
		return this.inputPhrase;
	}
	

	public String translate() {
		StringBuilder builder = new StringBuilder();
		
		String[] trimmedPhrase = inputPhrase.split(SPACE);
		
		for(int i=0; i<trimmedPhrase.length; i++) {
			if(i!=0) {
				builder.append(SPACE);
			}
			
			if(trimmedPhrase[i].contains("-")) {
				String[] trimmedWord = trimmedPhrase[i].split(DASH);
				StringBuilder wordBuilder = new StringBuilder();
				
				for(int j=0; j<trimmedWord.length; j++) {
					if(j!=0) {
						wordBuilder.append(DASH);
					}
					wordBuilder.append(translateSingleWord(trimmedWord[j]));
				}
				
				builder.append(wordBuilder.toString());
			}
			else {
				builder.append(translateSingleWord(trimmedPhrase[i]));
			}
		}
		
		return builder.toString();
	}
	
	
	private String translateSingleWord(String singleWord) {
		String initialPunctuation = "";
		String finalPunctuation = "";
		boolean isUpperCase = false;
		boolean isTitleCase = false;
		String translatedWord = "";
		
		if(singleWord.contains("'")) {
			String[] words = singleWord.split("'");
			
			return translateSingleWord(words[0]) + "'" + translateSingleWord(words[1]);
		}
		
		if(singleWord.equals(singleWord.toUpperCase())) {
			singleWord = singleWord.toLowerCase();
			isUpperCase = true;
		}
		
		if(isTitleCase(singleWord)) {
			singleWord = singleWord.toLowerCase();
			isTitleCase = true;
		}
		
		if(startsWithPunctuation(singleWord)) {
			initialPunctuation = getInitialPunctuationFromWord(singleWord);
		}
		
		if(endsWithPunctuation(singleWord)) {
			finalPunctuation = getFinalPunctuationFromWord(singleWord);
		}
		
		singleWord = cleanWord(singleWord);
		
		if(startsWithVowel(singleWord)) {
			if(singleWord.endsWith("y")) {
				translatedWord = singleWord + "nay";
			}
			else if(endsWithVowel(singleWord)) {
				translatedWord = singleWord + "yay";
			}
			else if(!endsWithVowel(singleWord)) {
				translatedWord = singleWord + "ay";
			}
		}
		else if(!singleWord.equals("")){
			int n = 0;
			n = numberOfStartingConsonantsOfWord(singleWord);
			translatedWord = singleWord.substring(n) + singleWord.substring(0, n) + "ay";
		}
		else {
			return NIL;
		}
		
		translatedWord = initialPunctuation + translatedWord + finalPunctuation;
		
		if(isUpperCase) {
			return translatedWord.toUpperCase();
		}
		
		if(isTitleCase) {
			return toTitleCase(translatedWord);
		}
		
		return translatedWord;
		
	}
	
	
	private boolean startsWithVowel(String s) {
		return s.startsWith("a") || 
				s.startsWith("e") || 
				s.startsWith("i") || 
				s.startsWith("o") || 
				s.startsWith("u");
	}
	
	
	private boolean endsWithVowel(String s) {
		return s.endsWith("a") || 
				s.endsWith("e") || 
				s.endsWith("i") || 
				s.endsWith("o") || 
				s.endsWith("u");
	}
	
	
	private boolean startsWithPunctuation(String singleWord) {
		return singleWord.startsWith("(");
	}
	
	
	private boolean endsWithPunctuation(String singleWord) {
		return singleWord.endsWith(".") ||
				singleWord.endsWith("!") ||
				singleWord.endsWith("?") ||
				singleWord.endsWith(",") ||
				singleWord.endsWith(";") ||
				singleWord.endsWith(")");
	}
	
	
	private String getFinalPunctuationFromWord(String singleWord) {
		int finalIndex = singleWord.length()-1;
		return singleWord.substring(finalIndex, finalIndex+1);
	}
	
	
	private String getInitialPunctuationFromWord(String singleWord) {
		return singleWord.substring(0, 1);
	}
	
	private String cleanWord(String singleWord) {
		int initialIndex = 0;
		int finalIndex = singleWord.length()-1;
		
		if(startsWithPunctuation(singleWord)) {
			initialIndex++;
		}
		
		if(!endsWithPunctuation(singleWord)) {
			finalIndex++;
		}
		
		return singleWord.substring(initialIndex, finalIndex);
	}
	
	
	private int numberOfStartingConsonantsOfWord(String singleWord) {
		int count = 0;
		int i = 0;
		boolean vowelFound = false;
		
		while(i<singleWord.length() && !vowelFound) {
			if(!startsWithVowel(singleWord.substring(i))) {
				count++;
			}
			else {
				vowelFound = true;
			}
			i++;
		}
		
		return count;
	}
	
	
	private boolean isTitleCase(String s) {
		if(s.length()<2) {
			return false;
		}
		
		Character firstChar = s.charAt(0);
		
		if(firstChar.equals(Character.toLowerCase(firstChar))) {
			return false;
		}
		
		for(int i=1; i<s.length(); i++) {
			Character c = s.charAt(i);
			if(!c.equals(Character.toLowerCase(c))) {
				return false;
			}
		}
		
		return true;
	}
	
	
	private String toTitleCase(String s) {
		Character firstChar = s.charAt(0);
		int finalIndex = s.length();
		
		return Character.toUpperCase(firstChar) + s.substring(1, finalIndex);
	}

}
